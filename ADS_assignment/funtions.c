#include<headerfiles.h>


void init_list(list_t *ele_t){
    ele_t->head=NULL;
    ele_t->count = 0;
}


bool_t is_list_empty(list_t *ele_t)
{
    return ( ele_t->head == NULL );
}

void display_list(list_t *ele_t){

        if( !is_list_empty(ele_t) )
        {
            node_t *trav = ele_t->head;
            printf("ELECTRONICS DEVICE ARE \n");
                do{
                    print_electronics(&trav->data);
                    trav = trav->next;
                 } while( trav != NULL);
                 printf("\n");
            printf("no. of nodes in a list are: %d\n", ele_t->count );

        }
        else
        printf("list is empty !!!\n");

}

void free_list(list_t *ele_t)
{
    //if list is not empty
    if( !is_list_empty(ele_t) )
    {
        while( !is_list_empty(ele_t) )
            //delete_node_at_last_position(ele_t);
            delete_at_fisrt_pos(ele_t);  

        printf("list freed successfully ...\n");
    }
    else
        printf("list is empty !!!\n");
}


node_t *create_node(electronics_t *electro){
 
    node_t *temp = (node_t *)malloc( sizeof(node_t) );

    if( temp == NULL )
    {
        perror("malloc() failed !!!\n");
        exit(1);
    }
    temp->data = *electro;
    temp->next = NULL;
    temp->prv = NULL;
    return temp;
}



