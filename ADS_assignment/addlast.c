#include<headerfiles.h>



void add_new_electronis_device_last(list_t *ele_t, electronics_t *eltro){

    node_t *newnode = create_node(eltro);

    if( is_list_empty(ele_t) )
    {
        ele_t->head = newnode;
        ele_t->count++;
    }
    else{
        node_t *trav = ele_t->head;

        while( trav->next != NULL )
        {
            trav = trav->next;
        }
        trav->next = newnode;
        newnode->prv = trav;
        ele_t->count++;
    }
}